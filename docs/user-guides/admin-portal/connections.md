---
title: Connections
---

## How to view connections on your network?

The connections page of the Admin portal is used to manage all device connections on the SentinelC network.

Several filters are available, such as the category of the device or its connection area.

It is possible to search for a device by its name, MAC address or to search for the account on which the devices are connected.

![Connections](connections/manage-connections.png)

To get more information, just click on the small triangle to the left of the mac address name to access the detailed information of the connection.

![Connections-details](connections/manage-connections-details.png)