---
title: Packet captures
---

## What is packet capture? 

Packet capture is the practice of gathering, collecting, and logging packets that pass through a device's network, regardless of the address to which the packet(s) are destined. By using it, each packet or defined subset of packets can be collected for analysis. A network administrator can then use the collected data and analyze it to, for example, monitor the bandwidth or traffic of his network.

With packet capture an administrator can therefore:

- Capture packets from main routers or remote access points to diagnose and resolve problems on the SentinelC network.

- Capture packets from a device that is connected to the SentinelC network by Wi-Fi or wire.

A packet capture consists of two main parts. On the one hand, a network adapter that connects the probe to the network. On the other hand, a software (Wireshark) that allows to log, consult or analyze the data collected by the device.

The results of packet captures are visible in the Admin portal:

![packet captures result](packet-captures/result-packet-captures.png)

You can then download the details of the capture by clicking on the red button with a download arrow.

Packet captured can also be restarted by clicking on the red button **Rescheduled this task**.

## How to launch a packet capture?

To perform a packet captures, log into the Admin portal, go to the Monitoring tab and click on the red **New packet capture** button.

![New packet captures](packet-captures/new-packet-capture.png)

Then fill in the fields of the form to target the packet capture. It can be done on a defined area or on the port of a device. It can also target a device connected to the network.

A link to the Wireshark documentation page will allow you to add filters to the packet capture.

Wireshark is a packet analyzer software. It is used in troubleshooting and analysis of computer networks.

## How do I start a packet capture on a specific device connected to the network?

There are 3 ways to perform a packet capture:

- On the packet captures page.
- On the devices page.
- On the connections page.

### Packet captures page.

Once you have filled in the chmaps in the packet capture form. Click **Apply** to start the capture on the specified device.

The capture is then launched, a redirection to the tasks page with the progress of the packet capture is made.

![Task packet captures](packet-captures/task-packet-capture.png)

It is also possible to edit a pending packet capture by clicking on the red pencil next to task ID.

### Devices page.

To monitor a device that is connected to the SentinelC network for example to a private zone, you must first identify the area in which the device is connected and its MAC address or name.

In this example, we have a network device with a SentinelC router and an access point. The identified device is a computer that is wired to port 3 of the router.

Log into the administrative dashboard, go to the Device section. Then identify the computer. Open the device details.

Click on the red **Capture Packets** button.

![Device packet captures](packet-captures/manage-device-details.png)

### Connections page.

Log into the Admin portal, go to the Connections tab and click on the small arrow to the right of the mac address of the target device. 

Click on the red **Capture Packets** button.

![connection packet captures](packet-captures/manage-connections-details.png)
