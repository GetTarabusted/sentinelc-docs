---
title: Overview
---

The Locations page of the Admin portal is used to manage all SentinelC account locations.

It is possible to search for a location when there are multiple locations.

![Locations](img/manage-location.png)


## Create a location

To create a location, click on the red **Create a new location** button at the top right.

Then you just have to fill in the form with the account and contact information in order to create the new location.

![Create-locations](img/create-location.png)

To get more information, just click on the location's name to access its detailed information.

![Locations](img/manage-location-details.png)

For more action on the location, click on the red arrow next to the location name.

![Locations](img/manage-location-action.png)

## Edit location

You can edit an account by clicking on the Edit button.

![Edit locations](img/edit-location.png)

## Delete location

You can delete location by clicking on the Delete button. Deletion will result in the destruction of the location and all associated.configurations.

![Delete locations](img/delete-location.png)