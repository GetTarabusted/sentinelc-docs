---
title: Wi-Fi networks
---

The Wi-Fi networks section shows the different Wi-Fi of the account.

![Wi-Fi](img/wi-fi.png)

It is possible to add a new one by clicking on the link **Add a new Wi-Fi**.

![Add Wi-Fi](img/add-wi-fi.png)


