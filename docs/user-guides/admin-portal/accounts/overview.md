---
title: Overview
---

The accounts page of the Admin portal is used to manage all SentinelC accounts.

![Create accounts](img/manage-account.png)

It is possible to search for an account when there are several accounts.

## Create a account

To create an account, click on the red **Create Account** button at the top right.

Then you just have to fill in the form with the account and contact information in order to create the account.

![Create accounts](img/create-account.png)

To get more information, just click on the account name to access the detailed information for that account.

![Accounts](img/manage-account-details.png)

For more action on the account, click on the red arrow next to the account name.

![Create accounts](img/action-account.png)

## Edit account

You can edit an account by clicking on the Edit button.

![Edit accounts](img/edit-account.png)

## Delete account

After click to delete account, you have 3 possibilities for your appliance:

- [Return to operator](/docs/user-guides/admin-portal/appliances/operations/#return-to-operator)

    The account is deleted as well as the networks, configurations or data related to this account. all your appliances zill be returned to SentinelC inventory and administrator will no longer own them.
    
- **Keep** 

    This action enables you to keep ownership of your appliances. The account is not deleted as well as the associated appliances. The other data will be deleted.

- [Transfer to](/docs/user-guides/admin-portal/appliances/operations/#transfer-to)

    The account is deleted, your equipment will be transferred to the account you choose.

![Delete accounts](img/delete-account.png)
