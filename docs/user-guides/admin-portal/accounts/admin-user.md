---
title: Admin user
---

The admin user section shows the different users of the account.

![Admin user](img/admin-user.png)

It is possible to create a new one by clicking on the link **Create new admin profile**.

![Create admin](img/create-admin.png)

For more details on the administrators, refer to [Administrators](/docs/user-guides/admin-portal/administrators/).