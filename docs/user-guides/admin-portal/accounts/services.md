---
title: Services
---

The services section shows the different account services that are deployed.

![Services](img/services.png)

It is possible to perform different actions on the services: Start, Stop, Uninstall, Parameters. 

For more details on the services, refer to [services](/docs/user-guides/admin-portal/appliances/embedded-services/).

