---
title: Incidents
---

The recent open incidents section allows you to view the latest incidents that have occurred on the account. A link to the incident will detail the incident. 

![Incidents](img/incidents.png)

For more details on the incidents, refer to [Incidents](/docs/user-guides/admin-portal/incidents/).