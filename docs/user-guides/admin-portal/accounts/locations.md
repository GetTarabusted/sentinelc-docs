---
title: Locations
---

The location section shows the different locations of the account.

![Locations](img/locations.png)

It is possible to create a new one by clicking on the link **Create a new location**.

![Create locations](img/create-locations.png)

For more details on the locations, refer to [Locations](/docs/user-guides/admin-portal/locations/overview/).

