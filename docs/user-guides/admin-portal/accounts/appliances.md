---
title: Appliances
---

The appliances section shows the different appliances of the account.

![Appiances](img/appliances.png)

It is possible to claim one by clicking on the link **Claim appliance**.

![Claim appliances](img/claim-appliances.png)

For more details on the appliances, refer to [Appliances](/docs/user-guides/admin-portal/appliances/overview/).