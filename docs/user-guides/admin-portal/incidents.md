---
title: Incidents
---

## How to view incidents on your network?

Incidents trigger a mandatory notification in the SentinelC application as well as an email notification.

You can see the incidents appear on the account page in the open incident column.

![Account-open-incidents](incidents/account.png)

Or even on the rentals page. Each incident is clickable to be redirected to the incident details.

![Location-open-incidents](incidents/location.png)

## Manage incidents on your network.

The incidents page of the administrative dashboard is used to manage all these incidents on the SentinelC network. By default the page filters these devices on open incidents.

It is also possible to search for an incident, a piece of appliance or a location for example when there are several.


![Incidents](incidents/manage-incidents.png)

To obtain more information, simply click on the incident ID to access the detailed information of the incident.

![Incidents-details1](incidents/manage-incidents-details1.png)

On the incident page, you can change the priority of the incident, change its status, as well as add support notes. You can also assign the incident to a member of the support team.

All actions taken on the incident will be noted in the History section.

![Incidents-details2](incidents/manage-incidents-details2.png)