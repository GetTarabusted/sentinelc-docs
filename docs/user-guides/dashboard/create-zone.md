---
title: Create zone
---


## How add a zone?

In order to create a zone, you need to log into the SentinelC application.

Then go to the **location** page of your router and select **Zones**.

![Location](./add-ap/locations.png)

On the Zone page, click **Add New Zone**.

![Zone](./create-zone/zones.png)

Fill in the required fields for the creation of the zone.

If you want to have control over the devices that connect to your network, select in the access policy section: **restricted**, otherwise select open.

![Add zone](./create-zone/add-zone.png)

Click on **Add** to add the new zone.

![Confirm add zone](./create-zone/confirm-add-zone.png)

A confirmation that the zone has been added is displayed, and you are redirected to the zones page.

![zone-page](./create-zone/zones-list.png)

## Edit the ports of an appliance to assign them a zone.

To know the status of the ports of your device, refer to the [Ports status](/docs/user-guides/dashboard/ports-status) page.

In the information page of the router, at the very bottom you will see the number of available and used ports, just click on Edit.

![Ports edit](./create-zone/ports-edit.png)

Then select a drop-down list on the port for which you want to assign a new zone.

![Ports edit zone](./create-zone/ports-edit-zone.png)

Click on **Apply** and then in the popup, click on **Ok** to save your changes.