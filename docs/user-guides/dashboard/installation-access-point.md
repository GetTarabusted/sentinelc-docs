---
title: Installation access point
---

## How to install and connect an access point?

### Step 1 - Add an Access point.

On the Location page, select **Add Access Point** in the *Add* section.

![Location](./add-ap/locations.png)

Enter the product key and click to **Continue**.

![product-key](./add-ap/product-key.png)

Enter your location and select Wi-Fi network you want to extend. Click to **Install** for to start the installation.

![Add access point](./add-ap/add-access-point.png)

If the Access Point is offline, a popup will indicate to connect your Access point.

![Access point offline](./add-ap/access-point-offline.png)

After your confirmation or if the Access Point is online, you have a confirmation of its installation.

![Access point installed](./add-ap/access-point-installed.png)

### Step 2 - Configure your ports.

First check if you have any ports available. Click on your location and select the red arrow next to your location name.

![organization](./add-ap/organization.png)

On the location page, select **Appliances**.

On the appliance page, click on **Edit** next to the appliance name to edit it.

![Appliances edit](./add-ap/appliances-edit.png)

Then go to the bottom of the appliance page, click on **Edit** and then select a drop-down list on the port you want to assign your access point to, and select **Access Point**.

![Appliances edit port](./add-ap/appliances-edit-port.png)

Click on **Apply** and then in the popup, click on **Ok** to save your changes.

### Step 3 – Physically plug in the device to authorize it on your network.

Once the port is selected and configured, please connect your access point to this port.

Then you need to authorize the device to connect to the network. To do so, go back to the location page of your network.

You will receive an authorization request: Waiting for permission.  Click on the grey arrow on the right of the Devices section.
