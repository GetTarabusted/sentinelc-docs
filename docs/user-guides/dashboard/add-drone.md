---
title: Installation drone
---

## How to install and connect a drone?

### Step 1 - Add a drone.

On the Location page, select **Add Drone** in the *Add* section.

![Location](./add-drone/locations.png)

Enter the product key and click to **Continue**.

![product-key](./add-drone/product-key.png)

Enter your location. Click to **Install** for to start the installation.

![Add drone](./add-drone/add-drone.png)

You have a confirmation of its installation.

![Drone installed](./add-drone/drone-installed.png)

### Step 2 - Configure your ports.

First check if you have any ports available. Click on your location and select the red arrow next to your location name.

![organization](./add-ap/organization.png)

On the location page, select **Appliances**.

On the appliance page, click on **Edit** next to the appliance name to edit it.

![Appliances edit](./add-ap/appliances-edit.png)

Then go to the bottom of the appliance page, click on **Edit** and then select a drop-down list on the port you want to assign your drone to, and select **Private**.

![Appliances edit port](./add-ap/appliances-edit-port.png)

Click on **Apply** and then in the popup, click on **Ok** to save your changes.

### Step 3 – Physically plug in the device to authorize it on your network.

Once the port is selected and configured, please connect your drone to this port.

