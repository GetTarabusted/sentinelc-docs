---
title: Password account
---


## How to modify your password account?

Connect to SentinelC dashboard and click to the account menu.

![User menu](./password-account/account-menu.png)

Select **Modify password**.

![User settings](./password-account/modify-password.png)

Enter actual password and new password.

You can also change the password on the login page of the application. You can reset it by clicking on the link Forgot password.