---
title: Extend Wi-Fi
---


## How to extend Wi-Fi?

Click on your location and select the red arrow next to your location name.

![organization](./add-ap/organization.png)

On the Location page, select **Wi-Fi** in the *View* section.

![Location](./add-ap/locations.png)

You will then be redirected to the Wi-Fi list. select the Wi-Fi you want to extend. Click to **More détails**, and select **Edit Wi-Fi**.

![Edit Wi-Fi](./extend-wi-fi/edit-wi-fi.png)

Now, select appliances to broadcast this Wi-Fi and click to **Apply** to expend your Wi-Fi network.

![Extend Wi-Fi](./extend-wi-fi/extend-wi-fi.png)