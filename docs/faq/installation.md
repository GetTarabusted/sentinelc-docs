---
title: Installation
hide_table_of_contents: true
---

import TOCInline from '@theme/TOCInline';


<TOCInline toc={
    toc
  } />

<div className="questions-title">

### Where can I find the registration key for my router?

Check under the SentinelC router, the registration key is written there.

### Why do I need to create a user account?

It is important to create a user account in order to access the SentinelC application.

Your user account, if it is an administrator, will allow you to add new Wi-Fi networks, to manage your users and to see the usage statistics.

### I have installed my SentinelC router but I am not receiving the activation email. What should I do?

1. Make sure you have entered the correct email address.
2. Make sure that the email is not received in your spam box.
3. Simply send another activation email.


### The LED indicating that the router is powered on is off. What does this mean?

For the SentinelC-A3P model, [see data sheet](https://www.sentinelc.com/faq/sentinelc-a3p/).

1. Verify that the charger power cord is connected and powered on at the back of the SentinelC router.
2. Ensure that power is being supplied to the charger.
3. If the charger is not supplying power then it may be defective, please contact technical support.

### When updating the router, the installation suddenly freezes. What should I do?

For the SentinelC-A3P model, [see data sheet](https://www.sentinelc.com/faq/sentinelc-a3p/).

First, please check the following three points:

- The connections are correctly made: Ethernet, power, connection to the modem.
- Then make sure that all three LEDs are lit green. Refer to the LED documentation for more information.
- Check that your modem is working.

If the 3 points above are checked and the update is still blocked, you must restart the router, to do this unplug the power cord and plug it back in.

If the problem persists, please contact our technical support.

### What is the difference between a master router and an access point?

A master router is used to create and manage your Wi-Fi networks. Your master router may not have a radio, so it will not broadcast a Wi-Fi network.

An access point is used to extend your wired or Wi-Fi network already created with your main router. Your access point may not have a radio, so it will not transmit a Wi-Fi network.

### What does location mean in your application?

The location indicates the physical location where you install the SentinelC router. By giving it a meaningful name, it's easy for you to identify your Wi-Fi, your appliance, and your connected devices.

### Can I install more than one master router per location?

No more than one master router can be installed per location.

Users can only extend their Wi-Fi networks to increase the range of their networks by adding access points. To add access points, follow this link which explains how to add an access point.

However, if the user needs to create new and separate networks, he can create a new location and add a master router to meet this need.


### How to diagnose an offline device during the initial setup?

In order to diagnose a device that appears to be offline, you need to check its local status.

To do this, make sure your SentinelC device is turned on, then use your mobile device to scan for Wi-Fi networks, you will then see a Wi-Fi named SentinelC followed by the serial number of your device.

To find the serial number and registration key of your device, go here.

Select this Wi-Fi and enter your device's registration key as the Wi-Fi password.

Once connected to the Wi-Fi, launch your Internet browser and type in http://192.168.99.1:9888 as the address, you will then arrive at the device status page and you can see its status.

image

### May i install more than one router on the same location?

You cannot add a second router for the same location, however it is possible to add a router if you have multiple locations. 

### How do I connect to the internet with a static IP?

Connect to your SentinelC router by following the [local web status page](/docs/technical-guides/local-web-status-page#connect-to-the-internet-with-a-static-ip).

In the local status page of your device, go to the very bottom to the section **Edit WAN connection**.

### How do I connect to the Internet via a Wi-Fi link?

Connect to your SentinelC router by following the [local web status page](/docs/technical-guides/local-web-status-page#connect-to-the-internet-via-a-wi-fi-link).

On your device's local status page, go to the very bottom to the **Edit WAN Connection** section, under the Wi-Fi tab. Enter the Wi-Fi information and click **Send Configuration**.

</div>