---
title: WAN configuration
---

Internet access configuration in the SentinelC OS is done via the `/ data / chinook / wan_config` file.

This file is deposited via:

- SSH
- Local console
- Via the on-board configuration web server (web_config.py): See [Local web status page](../local-web-status-page)


## Configuration format

This is a JSON configuration.

- `name`: The name of the network interface to use or create.
  - We can leave empty (null) or omit. The system will default to the first ethernet interface detected.
  - If 2nd level, we can use the placeholder `%` in the name, which will be replaced by the name of the parent interface.
- `type`: The type of interface to create. If empty, the interface must already exist and will not be created.
  - Each type of interface can receive a list of additional parameters. See [Types of interfaces] (# types-dinterfaces).
- `ip`: An object that indicates how to perform the IP configuration of the interface. See [IP configuration] (# configuration-ip).
- `mtu`: The Maximum Transfer Unit, 1500 bytes by default.
- `usage`: The usage that we want to force at the interface. Valid choices are: WAN or TRUNK.
  - In bridge mode, you must at least provide a TRUNK.
  - In router mode, you must at least provide a WAN.
- `subinterfaces`: List of subinterfaces to configure.

### Types of interfaces

#### Type default

Corresponds to type null.

The interface must already exist. It is therefore normally a real interface initialized by the kernel.

#### Type wifi

A wifi connection which will be configured via wpa_supplicant.

The `wifi` configuration object contains the settings for the" network "of wpa_supplicant.

Example:


``` js
{
  "name": "wlan0",
  "type": "wifi",
  "Wireless": {
    "ssid": "My Network",
    "psk": "xxxxxxxxx"
  },
  "ip": {
    "type": "DHCP"
  }
}
```

#### Type vlan

Only valid as a subinterface. The additional configuration asks to provide the vlan ID.

``` json
{
  "interface": "eth0",
  "subinterfaces": [
    {
      "type": "vlan",
      "name": "eth0.1",
      "vlan": {
        "id": 1
      },
      "ip": {
        "type": "DHCP"
      }
    }
  ]
}
```

### IP configuration

This sub-section controls how the IP address, DNS and default gateway will be set for the connection.

#### IP type DHCP

Assign an IP to this interface via DHCP. You may also obtain DNS and default gateway from the DHCP server.

A standard DHCP connection where you want to obtain the DNS and default gateway would look like:

```js
{
  "interface": "eth0",
  "ip": {
    "type": "DHCP",
    "dns": {
      "enable": true,    // this is the default, no need to specify it
      "use_dhcp": true,  // this is the default, no need to specify it
      "name_servers": [
        "8.8.8.8",       // even if use_dhcp is true (default), you still need fallback name_servers
        "8.8.4.4"        // in case the dhcp server does not provide one. These are the defaults.
      ]
    }
  }
}
```

The above configuration is equivalent to:

```json
{
  "interface": "eth0",
  "ip": {
    "type": "DHCP",
  }
}
```

If you want your ip address and default gateway to be assigned using DHCP but you
want to control the DNS server explicitly:

```json
{
  "interface": "eth0",
  "ip": {
    "type": "DHCP",
    "dns": {
      "use_dhcp": false,
      "name_servers": [
        "1.1.1.1"
      ]
    }
  }
}
```

This would cause the DNS server to be always set to 1.1.1.1. The DHCP client will
not request nor use DNS servers configured from the DHCP server.

#### DHCP request_gateway

A special option request_gateway can be set on a DHCP connection if you do not want to request or use the default gateway using DHCP.

This is a special case where the connection is not the main internet connection, and thus you do not want to set it as the default route.

This is usually combined with disabling DNS configuration since your DNS servers are probably coming from elsewhere also.

```json
{
  "interface": "eth0",
  "ip": {
    "type": "DHCP",
    "request_gateway": false,
    "dns": {
      "enable": false
    }
  }
}
```


#### IP type STATIC

Assign a static IP to this interface.

- `ip`: The IP address and the subnet in CIDR format. Example: 192.168.0.1/24.
- `gateway`: The default gateway.

Example:

```json
{
  "interface": "eth0",
  "ip": {
    "type": "STATIC",
    "ip": "192.168.1.45/24",
    "gateway": "192.168.1.1",
    "dns": {
      "name_servers": [
        "192.168.1.1"
      ]
    }
  }
}
```

This would use 192.168.1.1 as the gateway and name server for the connection.


## Examples

### Factory default

Special mode where we try to reach the internet via a vlan 1 and non-vlan packages.

This allows it to be plugged into a normal access port or into a trunk port.


``` json
{
  "name": null,
  "usage": "WAN",
  "ip": {
    "type": "DHCP"
   },
  "subinterfaces": [
    {
      "name": "% .1",
      "type": "vlan",
      "usage": "WAN",
      "vlan": {
        "id": 1
      },
      "ip": {
        "type": "DHCP"
      }
    }
  ]
}
```


### AP / Bridge mode

``` json
{
  "name": null,
  "usage": "TRUNK"
}
```

### Router / firewall mode with DHCP

``` json
{
  "name": null,
  "usage": "WAN",
  "ip": {
    "type": "DHCP"
  }
}
```


### Router / firewall with static IP

``` json
{
  "name": null,
  "usage": "WAN",
  "ip": {
    "type": "static",
    "ip": "192.168.1.10/24",
    "gateway": "192.168.1.1"
  }
}
```


### Wi-Fi with PSK

A wifi connection with PSK via `wlan0` and which configures the IP via DHCP.

``` js
{
  "name": "wlan0",
  "type": "wifi",
  "usage": "WAN",
  "Wireless": {
    "ssid": "My Network",
    "psk": "xxxxxxxxx"
  },
  "ip": {
    "type": "DHCP"
  }
}
```

### PPPoE over vlan 35

Bell FIBE

``` js
{
  "mtu": 1512, // 1500 + 8 bytes for pppoe + 4 bytes for vlan id
  "subinterfaces": [
    {
      "type": "vlan",
      "name": "% .35",
      "mtu": 1508,
      "vlan": {
        "id": 35
      },
      "subinterfaces": [
        {
          "name": "ppp0",
          "type": "ppp",
          "usage": "WAN",
          "mtu": 1500,
          "ppp": {
            "user": "b1xxxxxx",
            "peer": "peer",
            "secret": "password"
          }
        }
      ]
    },
  ],
}
```

### FUTURE: Mesh WiFi

Create an encrypted 802.11s mesh wifi.

Inside the mesh wifi, create a tunnel towards the main router (10.88.0.1) using GRETAP.

The tunnel will be used as a trunk to carry all VLAN traffic.


``` js
{
  "name": "wlan0",
  "type": "wifi",
  "mtu": 1528, // 1500 + 24 bytes (gretap) + 4 bytes (vlan). 802.11 frames have a max mtu of 2300 so this is fine.
  "Wireless": {
    "ssid": "mesh net",
    "mode": 5,
    "frequency": 2437,
    "key_mgmt": "SAE",
    "psk": "very secret password"
  },
  "ip": {
    "type": "STATIC",
    "ip": "10.88.0.2/24",
    "gateway": "10.88.0.1"
  },
  "subinterfaces": [
    {
      "type": "gretap",
      "name": "% .gretap",
      "mtu": 1504,
      "gretap": {
        "local": "10.88.0.2",
        "remote": "10.88.0.1"
      },
      "usage": "TRUNK"
    }
  ]
}
```
