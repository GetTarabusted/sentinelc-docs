---
title: Serial port connexion

---

## Connection to the SentinelC router in serial terminal:

- Connect the USB to Serial cable between the router and the computer
- Open a terminal
- List the name of the router:
`ls /dev/cu.usbserial- *`
- Connection to the router (Use the name listed above):
`screen /dev/cu.usbserial-XXXXXXXX 115200 –L`

Check the Firmware version:
`cat /etc/mender/artifact_info`

Update Router manual: `mender -rootfs UPDATE URL`

Restart:
`reboot` 