---
title: Subscribing to feeds
---

:::note
This page covers the subscription to custom/third party application feeds. If you simply want to install an application from the official feed, refer to the [Embedded services](/docs/user-guides/admin-portal/appliances/embedded-services) and the [Service library](/docs/user-guides/admin-portal/service-library) pages of the user guides.
:::

## What is an application feed?

The SentinelC OS supports embedding additional apps and services using Podman containers.

The recipes to install these services are published as a JSON file available online. To susbscribe to a feed, just configure its URL in your cloud controller instance. The feed will be automatically refreshed periodically.

:::warning
The SentinelC team only publishes and support applications included in the official, pre-configured feed. Third party feeds are not audited.
Even though applications are sandboxed using linux containers, running untrusted code from an untrusted source exposes you to many security risks.
:::


## How do I add a feed?

- Access the Back-end admin site of your SentinelC instance located at `https://api.sentinelc.example.com/admin/`
  - See [Initial login](/docs/controller/initial-login/) for the exact location and password of your installation.
- Navigate to the "Repositories" tab under "Pods".
- Select "Add repository" and fill the information with the name of your feed and the URL of the json feed.

These information should be provided by the authors of the custom feed you are trying to import.

Refer to [Publishing your own apps](../publishing-your-own-apps) for details on how to publish your own feed.


![Django admin page](sentinelc-repository-django.png)

Once the repository is added, it will automatically update itself each hour to import the latest updates.

You can also force a refresh using the admin action "Update Repository(ies)" from the list page.
