---
title: Technical overview
---

The SentinelC platform supports embedding additional applications or services that can be packaged as linux containers.

Under the hood, the SentinelC OS uses the Podman container engine to run containers and pods. An application is mostly a podman/kubernetes compatible yaml manifest template and some extra metadata such as a name, description and input parameters.

The following diagram shows the main concepts involved in the services system.

![diagram](sentinelc-service-diagram.png)

- An admin registers a repository inside the SentinelC API.
- The API fetch service definitions from the appfeed and imports it
- A user uses the service definitions to install an app to an appliance
- The app installation script is sent to the appliance and installed using podman (container)
- The API creates a host (virtual Mac Address) for the app instance and
- A static IP address is assigned to the app on first start.
- The app can be accessed with the cloud proxy, the local ports or a web-based terminal depending on the settings chosen at installation.

## Service statuses

Service statuses are displayed on the Embedded services page and the appliance details page.
This status is high-level and is used to manage the life cycle of the service.
Each service can have one of the following statuses:

| Status        | Description                                         |
|---------------|-----------------------------------------------------|
| Available     | The service is installed and ready to take actions. |
| Not available | The service is not available to take actions.       |
| Error         | The service has failed.                             |

In addition to the main service status, a current state is also shown to give more information
about the actual status of the service.
Each service can have one of the following current statuses:

| Current Status           | Description                                                          |
|--------------------------|----------------------------------------------------------------------|
| Ready                    | The service has been installed and is ready to start.                |
| Running                  | The service is alive and currently running.                          |
| Stopped                  | The service has been stopped by the user.                            |
| Missing                  | The service has not been reported by the appliance.                  |
| Uninstalling             | An uninstall task has been sent to the appliance.                    |
| Stopping                 | A stop task has been sent to the appliance.                          |
| Starting                 | A start task has been sent to the appliance.                         |
| Installing               | An install task has been sent to the appliance.                      |
| Updating parameters      | An update parameters task has been sent to the appliance.            |
| Service failed           | The appliance has reported that the service has failed.              |
| Start failed             | A start task has failed.                                             |
| Stop failed              | A stop task has failed.                                              |
| Install failed           | An install task has failed.                                          |
| Uninstall failed         | An uninstall task has failed.                                        |
| Update parameters failed | An update parameters task has failed.                                |
| Unknown                  | The appliance is offline or the service [disk](/docs/user-guides/admin-portal/appliances/disks/) is missing. |

![services-statuses](service-all-statuses.png)
