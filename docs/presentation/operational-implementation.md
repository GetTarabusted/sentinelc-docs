---
title: Operational implementation
---

Here are some examples of operational implementation. This list is not exhaustive as many combinations or alternatives are possible.

- I am an operator, I sell unconfigured SentinelC devices that customers must configure, install and administer themselves.
    - The customer has to do the onboarding of the equipment which involves creating an account and a network location. He will have to define the access zones, Wi-Fi and wired connections of his network and perform regular management.
    - The operator administers the equipment inventory, verifies the proper functioning of the platform and provides the necessary technical support to customers. He/she ensures that the devices are updated and that the client applications are working properly.
   
   Example: 
   
   Joe has a garage company distributed throughout the region. He wishes to operate the system and delegate the operations. He gives access to the different accounts he has created to local managers so that they can manage the network.
    He therefore creates branches (accounts) via the operator. Each account can have one or more locations. So administrator X has access to Branch 1, administrator Y has access to Branch 2, etc...
    Accounts can also be administered by one or more administrators if needed.
    The operator provides the necessary inventory to Joe for all his branches.

- I am an operator, I sell pre-configured and ready to plug in SentinelC equipment that will be administered by the customers
    - The account is defined according to the established scenario. Roles and accesses can be divided with several accounts.
    - The customer only has to connect his equipment according to the procedure.
    - The customer ensures the proper connection and operation of his secure network.
    - No need to configure zones or Wi-Fi, the operator provides the network key(s) in order to connect to the Wi-Fi.

    Example:

    David owns a hardware store. He is not familiar with Wi-Fi networks. 
    He wants to buy a ready-to-use network equipment that he can easily install in his store.
    He wants to manage his network as simply as possible while allowing his customers to enjoy a secure network.

- I am an operator and network administrator, I configure and install the compatible equipment of my choice and I perform the complete management.

    Example:

    John has an apartment in the city, and also a cottage in the country, he wants to structure his personal network on his 2 homes.
    He created an account with 2 locations: Apartment and Cottage.
    He is himself an administrator of the system. 
    He can therefore manage the network of his cottage directly from his apartment and vice versa.
