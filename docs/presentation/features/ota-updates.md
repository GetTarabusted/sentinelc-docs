---
title: Firmware updates

---

Lack of security updates and maintenance from network hardware vendors is a well known problem in the marketplace.

Even when manufacturers do provide security updates, they are often time cumbersome and risky to install by end-users. Users can also simply not be aware of available updates and the need for installing updates.

The SentinelC firmware is built from the ground up to be easy and safe to update.

### You are in control

- Full over-the-air (OTA) updates, centrally controlled from the admin portal.
- Install updates immediately or schedule them in the next maintenance window.

### Trust, but verify

- Official updates are cryptographically signed.
- The signature is verified on-device during the update process to detect any tampering.
- Advanced users and developers can fork, build and sign their own updates.

### Update with confidence

- Two versions are always available on disk.
- Updates are streamed to the inactive storage partition with no downtime, even on slow and unreliable networks.
- The system becomes unavailable only during the short time it takes to reboot into the new version.
- In the unlikely case the new version fails to initialize normally, an automated rollback to the previous version is performed.

## An in-depth look

Update binaries are distributed in the mender artifact format. The standalone [mender](https://mender.io/) utility is built into the firmware and is used to install and rollback updates.

The [cloud controller](/controller/intro.md) tracks installed versions, available updates and their distribution.

The firmware is split into 4 main parts:

- A small boot partition containing the critical parts required to boot the system.
- Two copies of the main system: A and B.
- A persistent data partition where persistent configurations can be preserved.

![Firmware high-level diagram](./firmware-highlevel.svg)

## Update process

The following status indicates the progress of the updates.

Available via the graphql API `under DeviceSchema.updateStatus`

| CODE        | FRANCAIS       | ANGLAIS     | NOTES                                                                                                                                           |
|-------------|----------------|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| AVAILABLE   | Disponible     | Available   | Only a privileged admin can release the update.                                                                                     |
| SCHEDULED   | Programmée     | Scheduled   | The Device.updateStatus.startTime field indicates the expected start time. The field canStart indicates if it is possible to force the start. 
| PENDING     | En attente     | Pending     | The update will start as soon as the device is ready.                                                                                         |
| STARTED     | Démarrage      | Starting    | The update has just started.                                                                                                    |
| DOWNLOADING | Téléchargement | Downloading | The update is downloaded. You can check Device.updateStatus.downloadProgress and downloadStartTime for details.                      |
| REBOOTING   | Redémarrage    | Rebooting   | The appliance reboots into the new version. rebootStartTime indicates the time when the reboot started.                                                                                                     |
| UP_TO_DATE  | À jour         | Up to date  | The appliance is up to date.                                                                                                                          |