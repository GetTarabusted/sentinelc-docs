---
title: Secops

---

The SentinelC Desktop administration console offers:

- **Packets capture**:
   Packet capture is the practice of gathering, collecting and logging packets that pass through a device's network, regardless of the address to which the packet(s) are destined. An administrator can therefore:
    - Capture packets from main router or remote access points to diagnose and troubleshoot problems on the SentinelC network.
    - Capture packets from a device connected to the SentinelC network via Wi-Fi or wire.

- **Wi-Fi Scan**:
    A Wi-Fi scan is used to detect signals from other Wi-Fi networks nearby.
Thus, it can detect other devices transmitting a Wi-Fi signal and will provide you with a list of all SSIDs present in the vicinity. The Wi-Fi scan will alert you when a potential rogue Wi-Fi discovered.

- **Ports scan**:
    Ports scan detects whether UDP or TCP ports are open on the network.