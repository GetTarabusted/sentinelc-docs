---
title: Preparing your Linux host

---

- This guide has been tested using a fresh, fully up-to-date Ubuntu 20.04 server.
- Ubuntu server VMs are available from your cloud provider of choice. Popular choices include, but are not limited to:
  - Amazon AWS EC2 VMs
  - Digital Ocean droplets
  - Azure VMs
  - Google Cloud Platform VMs
  - etc.

## Install distribution updates

Before proceeding make sure all available updates are installed.

```bash
apt update
apt dist-upgrade
```

## Required additional packages

### Docker and docker-compose

The docker.io and docker-compose packages provided by Canonical as part of Ubuntu are perfectly suitable and are recommended.

```bash
apt install docker.io docker-compose
```

### Wireguard

Communication between SentinelC devices and cloud services are encrypted using an auto-configured VPN based on wireguard. The VPN server and configuration is fully isolated from your Linux host, but the wireguard-tools package is required to generate a public/private key pair.

```bash
apt install wireguard-tools
```

### Java

The keycloak-config-cli tool is used to import an initial keycloak configuration file. This tool needs to be run only once and requires a `java` runtime.

```bash
apt install openjdk-11-jre-headless
```

The installed packages can be removed after you have completed the installation.

### Misc

The pwgen tool is used to generate random, initial passwords and secret keys.

```bash
apt install pwgen
```
