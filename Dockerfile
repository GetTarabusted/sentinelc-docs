FROM node:14
RUN mkdir /src
WORKDIR /src

# Improve caching by runnin npm install with only package.json
ADD package.json package-lock.json /src/
RUN npm ci

# Build site
ADD . /src
RUN npm run build

FROM nginx:1.14
COPY --from=0 /src/build /app
COPY nginx.conf /etc/nginx/conf.d/default.conf
